package com.example.gamefragement2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.example.gamefragement2.databinding.FragmentMinusGamesBinding
import kotlinx.android.synthetic.main.fragment_plus_games.*
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [plusGamesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MinusFragment : Fragment() {

    private lateinit var  binding: FragmentMinusGamesBinding
    private var resultText = "Please select an answer"
    private var thisScore = Score()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_minus_games, container,false)
        binding.resultText = resultText
        binding.score = thisScore

        Play()
        binding.apply {
            btnNextPlus.setOnClickListener {
                resultPlus.setText("Please Select an Answer")
                Play()
            }
        }
        binding.btnHomeMinus.setOnClickListener{
            it.findNavController().navigate(R.id.action_minusFragment_to_startGameMenuFragment)
        }
        return  binding.root
    }
    private fun Play() {
        binding.apply {
            val random1: Int = Random.nextInt(10) + 1
            textViewPlus1.setText(Integer.toString(random1))

            val random2: Int = Random.nextInt(10) + 1
            textViewPlus2.setText(Integer.toString(random2))

            val sum = random1 - random2

            val position: Int = Random.nextInt(3) + 1

            if (position == 1) {
                btnPlus1.setText(Integer.toString(sum))
                btnPlus2.setText(Integer.toString(sum - 1))
                btnPlus3.setText(Integer.toString(sum + 2))
            } else if (position == 2) {
                btnPlus2.setText(Integer.toString(sum))
                btnPlus1.setText(Integer.toString(sum - 2))
                btnPlus3.setText(Integer.toString(sum + 1))
            } else {
                btnPlus3.setText(Integer.toString(sum))
                btnPlus2.setText(Integer.toString(sum - 2))
                btnPlus1.setText(Integer.toString(sum + 3))
            }

            btnPlus1.setOnClickListener {
                if (btnPlus1.text.toString().toInt() == sum) {
                    resultText = "Correct"
                    thisScore.correct++
                    // correctPlus.text = (correctPlus.text.toString().toInt() + 1).toString()

                } else {
                    resultText = "Wrong"
                    thisScore.wrong++
                    // wrongResult.text = (wrongResult.text.toString().toInt() + 1).toString()
                }
                binding.invalidateAll()
            }
            btnPlus2.setOnClickListener {
                if (btnPlus2.text.toString().toInt() == sum) {
                    resultText = "Correct"
                    thisScore.correct++
                    //correctPlus.text = (correctPlus.text.toString().toInt() + 1).toString()

                } else {
                    resultText = "Wrong"
                    thisScore.wrong++
                    //wrongResult.text = (wrongResult.text.toString().toInt() + 1).toString()
                }
                binding.invalidateAll()
            }
            btnPlus3.setOnClickListener {
                if (btnPlus3.text.toString().toInt() == sum) {
                    resultText = "Correct"
                    thisScore.correct++
                    //correctPlus.text = (correctPlus.text.toString().toInt() + 1).toString()

                } else {
                    resultText = "Wrong"
                    thisScore.wrong++
                    //wrongResult.text = (wrongResult.text.toString().toInt() + 1).toString()
                }
                binding.invalidateAll()
            }
        }
    }


}